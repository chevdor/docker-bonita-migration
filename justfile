_init:
    #!/usr/bin/env bash
    set -e
    export DOCKER_REGISTRY=${DOCKER_REGISTRY:-docker.io}

docker-build: _init
    #!/usr/bin/env bash
    TMSP=$(date +'%Y-%m-%d')
    docker build \
        -t chevdor/bonita-updater \
        -t $DOCKER_REGISTRY/chevdor/bonita-updater \
        -t $DOCKER_REGISTRY/chevdor/bonita-updater:$TMSP \
        .
    docker images | grep bonita-updater

docker-push: _init
    #!/usr/bin/env bash
    TMSP=$(date +'%Y-%m-%d')
    docker push $DOCKER_REGISTRY/chevdor/bonita-updater:$TMSP
    docker push $DOCKER_REGISTRY/chevdor/bonita-updater
