#!/usr/bin/env sh

echo Fetching $ZIP

cd /tmp
wget $ZIP -O archive.zip                    # fetch the zip
unzip -q archive.zip                        # unzip
rm -f archive.zip                           # cleanup
ls -d bonita* | xargs -I {} mv {} folder    # rename the folder to a known name: 'folder'
rm -rf folder/bin/*.bat                     # remove junk
rm -rf /update/tool
mkdir -p /update/tool
mv folder/* /update/tool                    # move content
rmdir folder                                # cleanup

echo Done fetching the update tool
