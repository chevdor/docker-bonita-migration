FROM openjdk:17-alpine

RUN apk add gettext

WORKDIR /update
COPY *.adoc ./
COPY scripts/help /usr/local/bin/
COPY Config.properties.template /update
COPY scripts/* /update/bin/

ENV DB=postgres
ENV URL=jdbc:postgresql://postgres:5432/bonitadb
ENV DRIVER=org.postgresql.Driver
ENV USER=postgres
ENV PASSWD=secret
ENV POOL_SIZE_INIT=3
ENV POOL_SIZE_MAX=10
ENV BONITA_HOME=/opt/tomcat/bonita
ENV LOG_LEVEL=INFO
ENV ZIP=https://github.com/bonitasoft/bonita-platform-releases/releases/download/2023.2-u0/bonita-update-tool-3.3.0.zip
ENV TARGET=9.0.0

# VOLUME /update/Config.properties
ENV PATH="/update/bin:${PATH}"
WORKDIR /update/bin

CMD ["/usr/local/bin/help"]
